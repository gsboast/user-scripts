// ==UserScript==
// @name         YT REM
// @namespace    http://garrettboast.com/
// @version      0.1
// @description  Hides Watch Later / Liked Videos from the sidebar on YouTube. Currently English-only.
// @author       Garrett Boast <garrett@garrettboast.com>
// @match        https://www.youtube.com/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js
// ==/UserScript==

(function() {
    'use strict';
    window.jq = $.noConflict(true);
    var selector = 'a[title="Purchases"], a[title="Liked videos"], a[title="History"]';
    var f = setInterval(function() {
        var l1 = jq(selector).parent().remove().length;
        if(l1 > 0) console.log("removed", l1);
        if(l1 > 0) clearInterval(f);
    }, 100);


    setInterval(function() {
        var l2 = jq(selector).parent().remove().length;
    }, 2000);
})();