// ==UserScript==
// @name         FB AutoplayNuke
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.facebook.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    window.setInterval(function() {
        let vvids = document.getElementsByTagName('video');
        for(let i = 0; i < vvids.length; i++) {
            if(vvids[i].currentTime > (vvids[i].duration*0.5)) {
                let evaluater = new XPathEvaluator();
                let resolver = document.createNSResolver( document.documentElement );
                let iterator = evaluater.evaluate( ".//span[text()='Cancel']", vvids[i].parentElement.parentElement, resolver, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null );
                let result = "";
                try
                {
                    while( ( result = iterator.iterateNext() ) != null )
                    {
                        result.click();
                    }
                }
                catch(ex) {
                    return;
                }
            }
        }
    }, 1000);
})();