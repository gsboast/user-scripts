# README #

Here are a collection of user scripts for TamperMonkey that I use for various tasks around the web.

### What does this repository contain? ###

A summary of the scripts:

* **YT-REM** - Hides Watch Later / Liked Videos from the sidebar on YouTube
* **FB Video Speed** - Allows the controlling of video speed on Facebook